package kr.nextm.test

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.Log
import com.google.android.gms.tasks.Task
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.QuerySnapshot
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {

    data class Shop(
        val id: String = "",
        val count: Int = 1,
        val dateLastUpdated: String = ""
    )

    val TAG: String = javaClass.canonicalName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // Access a Cloud Firestore instance from your Activity

        val db = FirebaseFirestore.getInstance()

        buttonSave.setOnClickListener {

            require(shopId.isNotEmpty())
            require(date.isNotEmpty())

            val shop = Shop(shopId, count, date)

            db.collection("test/v1/shops")
                .add(shop)
                .addOnSuccessListener { documentReference ->
                    Log.d(
                        "TEST",
                        "DocumentSnapshot added with ID: " + documentReference.id
                    )
                }
                .addOnFailureListener { e -> Log.w(TAG, "Error adding document", e) }
        }

        buttonClear.setOnClickListener {
            editShopId.text.clear()
            editCount.text.clear()
            editDateLastUpdate.text.clear()
        }

        buttonLoad.setOnClickListener {

            db.collection("test/v1/shops")
                .get()
                .addOnCompleteListener { task: Task<QuerySnapshot> ->
                    if (task.isSuccessful) {
                        for (document in task.result!!) {
                            val shop = document.toObject(Shop::class.java)
                            Log.d(TAG, document.id + " => " + shop)
                            editShopId.setText(shop.id)
                            editCount.setText(shop.count.toString())
                            editDateLastUpdate.setText(shop.dateLastUpdated)
                        }
                    } else {
                        Log.w(TAG, "Error getting documents.", task.exception)
                    }
                }
                .addOnFailureListener { e -> Log.w(TAG, "Error get document", e) }
        }
    }

    val shopId get() = editShopId.text.toString()
    val count get() = editCount.text.toString().toInt()
    val date get() = editDateLastUpdate.text.toString()
}
